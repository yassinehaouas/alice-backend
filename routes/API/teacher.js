const express = require("express");
const router = express.Router();
const Teacher = require("../../models/Teacher");
const User = require("../../models/User");
const jwt_decode = require("jwt-decode");
const jwt = require('jsonwebtoken');
const cryptoRandomString = require('crypto-random-string');
const nodemailer = require('nodemailer');
const bcrypt = require('bcryptjs');
const config = require('config');


// @route    POST api/teachers
// @desc     Register Teacher
// @access   Public
router.post(
    "/",
    async (req, res) => {

        const { token, availability, languages, sponsorship,password } = req.body;

        let decoded = jwt_decode(token);
        console.log(decoded['user'].id);
        const userId = decoded['user'].id;
        try {
            //See if user exists
            let user = await User.findById(decoded['user'].id);
            if (!user) {
                console.log(user.enabled);
                return res.status(400).json({ errors: [{ msg: "user does not exist" }] });
            }

            const activation = cryptoRandomString({ length: 10 });
            teacher = new Teacher({
                user,
                availability,
                languages,
                sponsorship,
                activation
            });
           // send confirmation mail
            var transporter = nodemailer.createTransport({
                service: 'gmail',
                auth: {
                    user: 'appmailingtester@gmail.com',
                    pass: 'Challouf97'
                }
            });
            const mailOptions = {
                from: 'appmailingtester@gmail.com', // sender address
                to: user.email, // list of receivers
                subject: ' Teacher account Confirmation code', // Subject line
                html: '<p>Your confirmation code ' + activation + '</p>'// plain text body
            };
            transporter.sendMail(mailOptions, function (err, info) {
                if (err)
                    console.error(err)
                else
                    console.log(info);
            });

            //encrypt passwor(bcrypt)
            const salt = await bcrypt.genSalt(10);

            teacher.password = await bcrypt.hash(password, salt);
            console.log("mochkol hna");
            await teacher.save();

            //return jsonwebtoken
            const payload = {
                teacher: {
                    id: teacher.id,
                    userId: teacher.user
                }
            };

            jwt.sign(payload, config.get('jwtSecret'), {
                expiresIn: 360000
            }, (err, token) => {
                if (err) throw err;
                res.json({ token });
            });


        } catch (err) {
            console.log(err.message);
            res.status(500).send("server error");
        }
    }
);

// @route   PUT api/teachers
// @desc    put modify teacher by id
// @access  PUBLIC 
router.put('/addLanguages',
    async (req, res) => {

        const { token, Newlanguages } = req.body;

        let decoded = jwt_decode(token);
        console.log(decoded['teacher'].id,"okokok");
        let teacherId = decoded['teacher'].id;

        
        try {
            console.log("okokokokokok");
            let teacher = await Teacher.findById(teacherId);
            if (!teacher) {
                console.log("ok");
                return res.status(400).json({ message: 'there is no Teacher for this id' });
            }
            teacher = await Teacher.findByIdAndUpdate(
                { _id: teacherId },
                { $push: {languages: Newlanguages } },
                { new: true }
            );
            console.log("updated");
            res.json(teacher);
        } catch (error) {
            console.error(error.message);
            return res.status(500).send('server error');
        }
    }
);


module.exports = router;