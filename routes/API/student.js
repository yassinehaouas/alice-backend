const express = require("express");
const router = express.Router();
const Student = require("../../models/Student");
const User = require("../../models/User");
const jwt_decode = require("jwt-decode");
const jwt = require('jsonwebtoken');
const cryptoRandomString = require('crypto-random-string');
const nodemailer = require('nodemailer');
const config = require('config');
// @route    POST api/students
// @desc     Register Student
// @access   Public
router.post(
    "/",
    async (req, res) => {

        const { token, interests } = req.body;

        let decoded = jwt_decode(token);
        console.log(decoded['user'].id);
        const userId = decoded['user'].id;
        try {
            //See if user exists
            let user = await User.findById(decoded['user'].id);
            if (!user) {
                console.log(user.enabled);
                return res.status(400).json({ errors: [{ msg: "user does not exist" }] });
            }

            const activation = cryptoRandomString({ length: 10 });
            student = new Student({
                user,
                interests,
                activation
            });
           // send confirmation mail
            var transporter = nodemailer.createTransport({
                service: 'gmail',
                auth: {
                    user: 'appmailingtester@gmail.com',
                    pass: 'Challouf97'
                }
            });
            const mailOptions = {
                from: 'appmailingtester@gmail.com', // sender address
                to: user.email, // list of receivers
                subject: ' Student account Confirmation code', // Subject line
                html: '<p>Your confirmation code ' + activation + '</p>'// plain text body
            };
            transporter.sendMail(mailOptions, function (err, info) {
                if (err)
                    console.error(err)
                else
                    console.log(info);
            });

            // user.password = await bcrypt.hash(password, salt);
            console.log("mochkol hna");
            await student.save();

            //return jsonwebtoken
            const payload = {
                user: {
                    id: student.user
                }
            };

            jwt.sign(payload, config.get('jwtSecret'), {
                expiresIn: 360000
            }, (err, token) => {
                if (err) throw err;
                res.json({ token });
            });


        } catch (err) {
            console.log(err.message);
            res.status(500).send("server error");
        }
    }
);

// @route   PUT api/teachers
// @desc    put modify teacher by id
// @access  PUBLIC 
router.put('/addDesires',
    async (req, res) => {

        const { token, desires } = req.body;

        let decoded = jwt_decode(token);
        console.log(decoded['user'].id,"okokok");
        let userId = decoded['user'].id;

        
        try {
            console.log("okokokokokok");
            let user = await User.findById(userId);
            if (!user) {
                console.log("ok");
                return res.status(400).json({ message: 'there is no User for this id' });
            }
            user = await User.findByIdAndUpdate(
                { _id: userId },
                { $push: {interests: desires } },
                { new: true }
            );
            console.log("updated");
            res.json(user);
        } catch (error) {
            console.error(error.message);
            return res.status(500).send('server error');
        }
    }
);




module.exports = router;