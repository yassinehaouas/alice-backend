const express = require('express');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('config');
const router = express.Router();
const User = require('../../models/User');
const Student = require('../../models/Student');
const Teacher = require('../../models/Teacher');
const jwt_decode = require("jwt-decode");
var nodemailer = require('nodemailer');



// @route    api/auth/confirmation
// @desc    confirmation user account
// @access  Private
router.post('/confirmation', async (req, res) => {

    let decoded = jwt_decode(req.body.token);
        console.log(decoded['user'].id);
        const userId = decoded['user'].id;
    try {
        const user = await User.findById(userId);
        if (user.activation != req.body.activation) {
            return res.status(400).json({ errors: [{ msg: ' invalid confirmation code ' }] })
        }

        user.enabled = true;
        await user.save();
        res.json(user);
    } catch (error) {
        console.error(error.message);
        res.status(500).send('Server error');
    }
});

// @route    api/auth/confirmTeacher
// @desc    confirmation teacher account
// @access  Private
router.post('/confirmTeacher', async (req, res) => {
    let decoded = jwt_decode(req.body.token);
        console.log(decoded['user'].id);
        const userId = decoded['user'].id;
    try {
        const teacher = await Teacher.findOne({ user: userId });
        if (teacher.activation != req.body.activation) {
            return res.status(400).json({ errors: [{ msg: ' invalid confirmation code ' }] })
        }

        teacher.enabled = true;
        await teacher.save();
        res.json(teacher);
    } catch (error) {
        console.error(error.message);
        res.status(500).send('Server error');
    }
});

// @route    api/auth/confirmStudent
// @desc    confirmation student account
// @access  Private
router.post('/confirmStudent', async (req, res) => {
    let decoded = jwt_decode(req.body.token);
        console.log(decoded['user'].id);
        const userId = decoded['user'].id;
    try {
        const student = await Student.findOne({ user: userId });
        if (student.activation != req.body.activation) {
            return res.status(400).json({ errors: [{ msg: ' invalid confirmation code ' }] })
        }

        student.enabled = true;
        await student.save();
        res.json(student);
    } catch (error) {
        console.error(error.message);
        res.status(500).send('Server error');
    }
});

// @route   POST api/auth
// @desc    Authenticate user & get token
// @access  Public 
router.post('/',
    async (req, res) => {
        

        const { email, password } = req.body;

        try {
            //See if user exists
            let user = await User.findOne({ email });
            if (!user) {
                return res.status(400).json({ errors: [{ msg: 'invalid credentials' }] });
            }


            //match password
            const isMatch = await bcrypt.compare(password, user.password);
            if (!isMatch) {
                return res.status(400).json({ errors: [{ msg: ' invalid credentials' }] });
            }
            //return jsonwebtoken 
            const payload = {
                user: {
                    id: user.id
                }
            }
            jwt.sign(
                payload,
                config.get('jwtSecret'),
                { expiresIn: 360000 },
                (err, token) => {
                    if (err) throw err;
                    res.json({ token });
                }
            );

        } catch (error) {
            console.error(error.message);
            res.status(500).send('Server error');
        }




    });

    // @route   POST api/auth
// @desc    Authenticate user & get token
// @access  Public 
router.post('/loginTeacher',
async (req, res) => {
    

    const { email, password } = req.body;

    try {
        //See if user exists
        let user = await User.findOne({ email });
        if (!user) {
            return res.status(400).json({ errors: [{ msg: 'invalid credentials' }] });
        }
        console.log(user.id, user);
        //See if user exists
        const teacher = await Teacher.findOne({ user: user.id });
        console.log(teacher.languages);
        if (!teacher) {
            return res.status(400).json({ errors: [{ msg: 'invalid credentials' }] });
        }

        //match password
        const isMatch = await bcrypt.compare(password, teacher.password);
        console.log(isMatch);
        if (!isMatch) {
            return res.status(400).json({ errors: [{ msg: ' invalid credentials' }] });
        }
        //return jsonwebtoken 
        const payload = {
            teacher: {
                id: teacher.id,
                userId: teacher.user
            }
        }
        jwt.sign(
            payload,
            config.get('jwtSecret'),
            { expiresIn: 360000 },
            (err, token) => {
                if (err) throw err;
                res.json({ token });
            }
        );

    } catch (error) {
        console.error(error.message);
        res.status(500).send('Server error');
    }




});

// @route    api/auth/forgotpassword
// @desc    send email with link to reset password
// @access  Private
router.post('/forgotpassword', async (req, res) => {
    

        let email = req.body.email;
        try {
            //See if user exists
            let user = await User.findOne({ email });
            if (!user) {
                return res.status(400).json({ errors: [{ msg: 'User does not exist' }] });
            }

        //send confirmation mail
        var transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: 'appmailingtester@gmail.com',
                pass: 'Challouf97'
            }
        });
        const mailOptions = {
            from: 'appmailingtester@gmail.com', // sender address
            to: email, // list of receivers
            subject: 'Reset Password', // Subject line
            html: '<p> please click here to reset your password </p>'// plain text body
        };
        transporter.sendMail(mailOptions, function (err, info) {
            if (err)
                console.error(err)
            else
                console.log(info);
        });
        res.json(true);
    } catch (error) {
        console.error(error.message);
        res.status(500).send('Server error');
    }
});

// @route    api/auth/confirmation
// @desc    confirmation user account
// @access  Private
router.post('/resetpassword', async (req, res) => {


    const { newp, email, confirmp } = req.body;
    try {
        //See if user exists
        let user = await User.findOne({ email });
        if (!user) {
            return res.status(400).json({ errors: [{ msg: 'User does not exist' }] });
        }
        if(newp === confirmp){
            //encrypt passwor(bcrypt)
            const salt = await bcrypt.genSalt(10);

            user.password = await bcrypt.hash(newp, salt);

            await user.save();
            res.json(user);
        }else
        return res.status(400).json({ errors: [{ msg: 'password do not match' }] });
    } catch (error) {
        console.error(error.message);
        res.status(500).send('Server error');
    }
});


module.exports = router;