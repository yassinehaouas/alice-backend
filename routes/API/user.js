const express = require("express");
const router = express.Router();
const User = require("../../models/User");
const config = require('config');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const cryptoRandomString = require('crypto-random-string');
var nodemailer = require('nodemailer');
// @route    POST api/users
// @desc     Register user
// @access   Public
router.post(
    "/",
    async (req, res) => {

        const { firstName, lastName, email, phoneNumber, city, country, zipcode, birthDate, gender, password } = req.body;
        try {
            //See if user exists
            let user = await User.findOne({ email });
            if (user) {
                console.log(user.enabled);
                return res.status(400).json({ errors: [{ msg: "user is already registered" }] });
            }

            const address = {};
            address.city = city;
            address.country = country;
            address.zipcode= zipcode
            const activation = cryptoRandomString({ length: 10 });
            user = new User({
                firstName,
                lastName,
                email,
                phoneNumber,
                address,
                birthDate,
                gender,
                password,
                activation
            });
            //send confirmation mail
            var transporter = nodemailer.createTransport({
                service: 'gmail',
                auth: {
                    user: 'appmailingtester@gmail.com',
                    pass: 'Challouf97'
                }
            });
            const mailOptions = {
                from: 'appmailingtester@gmail.com', // sender address
                to: email, // list of receivers
                subject: 'Confirmation code', // Subject line
                html: '<p>Your confirmation code ' + activation + '</p>'// plain text body
            };
            transporter.sendMail(mailOptions, function (err, info) {
                if (err)
                    console.error(err)
                else
                    console.log(info);
            });
            //encrypt passwor(bcrypt)
            const salt = await bcrypt.genSalt(10);

            user.password = await bcrypt.hash(password, salt);

            await user.save();

            //reurn jsonwebtoken
            const payload = {
                user: {
                    id: user.id
                }
            };

            jwt.sign(payload, config.get('jwtSecret'), {
                expiresIn: 360000
            }, (err, token) => {
                if (err) throw err;
                res.json({ token });
            });


        } catch (err) {
            console.log(err.message);
            res.status(500).send("server error");
        }
    }
);


module.exports = router;