const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    gender: {
        type: String,
        required: true
    },
    address: {
        city: {
            type: String,
            required: true
        },
        country: {
            type: String,
            required: true
        },
        zipcode: {
            type: String,
        },
    },
    birthDate: {
        type: Date,
        required: true
    },
    phoneNumber: {
        type: Number,
        required: true
    },
    wallet: {
        type: Number,
        required: true,
        default: 0
    },
    translation: {
        type: [
            {
                description: {
                    type: String,
                    required: true
                },
                attachments: {
                    type: [String],
                    required: true
                },
                deadline: {
                    type: Date,
                    required: true,
                },
                price: {
                    type: Number,
                    required: true,
                }
            }
        ]
    },
    activation: {
        type: String
    },
    enabled: {
        type: Boolean,
        default: false
    },
    interests:{
        type:[String],
        required: true
    }
});
module.exports = User = mongoose.model('User', UserSchema);