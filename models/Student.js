const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const StudentSchema = new Schema({
    user:{
        type: Schema.Types.ObjectId,
        ref: 'users',
        required: true,
    },
    interests:{
        type:[String],
        required: true
    },
    activation: {
        type: String
    },
    enabled: {
        type: Boolean,
        default: false
    }
  }); 
  module.exports = Student = mongoose.model('Student', StudentSchema);