const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TeacherSchema = new Schema({
    user:{
        type: Schema.Types.ObjectId,
        ref: 'users',
        required: true,
    },
    languages:{
        type:[
            {
                title:{
                    type: String,
                    required:true
                },
                proficiency:{
                    type: String,
                    required: true
                },
                certificates:{
                    type: [String],
                },
                rating:{
                    type: Number,
                }
            }
        ]
    },
    availability:{
        type: {
            days:{
                type: [String],
                required: true,
            },
            from:{
                type: Date,
                required: true
            },
            to:{
                type: Date,
                required: true
            },
        }
    },
    sponsorship:{
        type: {
            sponsored:{
                type: Boolean,
                required: true
            },
            expireDate:{
                type:Date,
            }
        }
    },
    reviews:{
        type:{
            reviewer:{
                type: mongoose.Schema.Types.ObjectId,
                ref: 'users'
            },
            rate:{
                type: Number,
                required: true
            },
            comment:{
                type: String,
                required: true
            }
        }
    },
    activation: {
        type: String
    },
    enabled: {
        type: Boolean,
        default: false
    },
    password: {
        type: String,
        required: true
    }
  }); 
  module.exports = Teacher = mongoose.model('Teacher', TeacherSchema);